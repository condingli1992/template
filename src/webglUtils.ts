/**
 * @description 创建并编译一个着色器
 * @param {WebGLRenderingContext} gl WebGL上下文
 * @param {number} type 着色器类型 VERTEX_SHADER 或 FRAGMENT_SHADER
 * @param {string} source GLSL格式的着色器代码
 * @return {!WebGLShader} 着色器
 *
 */
export function createShader(gl: WebGLRenderingContext, type: number, source: string): WebGLShader {
  const shader = gl.createShader(type);
  if (!shader) throw 'Create shader failed';
  gl.shaderSource(shader, source);
  gl.compileShader(shader);
  const status = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
  if (!status) throw 'Could not compile shader: ' + gl.getShaderInfoLog(shader);
  return shader;
}

/**
 * @description 创建一个着色器程序
 * @param {WebGLRenderingContext} gl WebGL上下文
 * @param {WebGLShader} vertexShader 顶点着色器
 * @param {WebGLShader} fragmentShader 片段着色器
 * @return {WebGLProgram} 着色器程序
 *
 */
export function createProgram(gl: WebGLRenderingContext, vertexShader: WebGLShader, fragmentShader: WebGLShader): WebGLProgram {
  const program = gl.createProgram();
  if (!program) throw 'create program failed';
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);
  const status = gl.getProgramParameter(program, gl.LINK_STATUS);
  if (!status) throw 'program failed to link: ' + gl.getProgramInfoLog(program);
  return program;
}

/**
 * @description 通过script标签创建着色器
 * @param {WebGLRenderingContext} gl WebGL上下文
 * @param {string} scriptId script标签ID
 * @param {number}  shaderType 着色器类型
 * @return {WebGLShader} 着色器
 */
export function createShaderFromScript(gl: WebGLRenderingContext, scriptId: string, shaderType?: number): WebGLShader {
  const shaderScript = document.getElementById(scriptId) as HTMLScriptElement;
  if (!shaderScript) throw 'Unknow script element: ' + scriptId;
  const shaderSource = shaderScript.textContent || '';
  if (!shaderType) {
    if (shaderScript.type === 'x-shader/x-vertex') {
      shaderType = gl.VERTEX_SHADER;
    } else if (shaderScript.type === 'x-shader/x-fragment') {
      shaderType = gl.FRAGMENT_SHADER;
    } else if (!shaderType) {
      throw 'shaderType not set';
    }
  }
  return createShader(gl, shaderType, shaderSource);
}

/**
 * @description 通过Script标签创建着色器程序
 * @param  {WebGLRenderingContext} gl WebGL上下文
 * @param  {string} vertexScriptId
 * @param  {string} fragmentScriptId
 * @returns WebGLProgram 着色器程序
 */
export function createProgramFromScript(gl: WebGLRenderingContext, vertexScriptId: string, fragmentScriptId: string): WebGLProgram {
  const vertexShader = createShaderFromScript(gl, vertexScriptId, gl.VERTEX_SHADER);
  const fragmentShader = createShaderFromScript(gl, fragmentScriptId, gl.FRAGMENT_SHADER);
  return createProgram(gl, vertexShader, fragmentShader);
}
