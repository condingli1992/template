const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const paths = {
  src: path.join(__dirname, './src'),
  build: path.join(__dirname, './build'),
}

module.exports = {
  context: path.resolve(__dirname, 'app'),

  entry: {
    app: [path.resolve(paths.src, './index.ts')],
  },

  devtool: 'eval-source-map',

  output: {
    path: paths.build,
    publicPath: './',
    filename: '[name].bundle.js',
    sourceMapFilename: '[name].bundle.map',
    chunkFilename: '[name].chunk.bundle.js',
  },

  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.scss', '.css'],
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpackPlugin({
      title: 'WebGL',
      template: path.join(paths.src, './index.html'),
      hash: true,
      inject: 'body',
    }),
  ],

  devServer: {
    publicPath: 'http://localhost:8080',
    contentBase: './',
    compress: true,
    hot: true,
    port: 8080,
    historyApiFallback: true,
  },

  module: {
    rules: [
      {
        test: /\.(ts|js)$/,
        use: ['ts-loader'],
        include: [path.resolve(paths.src, './')],
      },
    ],
  },

  externals: {
    window: 'window',
  },
}
